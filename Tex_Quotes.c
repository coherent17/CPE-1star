//http://uva.onlinejudge.org/external/2/272.html
#include <stdio.h>
#include <string.h>

int main(){
    char str[1000];
    int i,j=0;
    while(gets(str)!=EOF){
        for(i=0;i<strlen(str);i++){
            if(str[i]=='\"'){
                j+=1;
                if(j%2==1){
                    printf("``");
                }
                else{
                    printf("\'\'");
                }
            }
            else{
                printf("%c",str[i]);
            }
        }
        printf("\n");
    }
    return 0;
}
