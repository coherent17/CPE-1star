//http://uva.onlinejudge.org/external/114/11461.html
#include <stdio.h>

int s[100001];

int main(){
    int i,a,b;
    //if the index is square numbers -> the value is 1
    for(i=1;i*i<100000;i++){
        s[i*i]=1;
    }
    //let the value of the index represent how many square numbers
    for(i=1;i<100001;i++){
        s[i]+=s[i-1];
    }
    while(scanf("%d %d",&a,&b)!=EOF){
        if(a==0&&b==0) break;
        printf("%d\n",s[b]-s[a-1]);
    }
    return 0;
}
