//http://uva.onlinejudge.org/external/108/10812.html
#include <stdio.h>

int main(){
    int s,d,a,b;
    int n;
    int i;
    scanf("%d",&n);
    for(i=0;i<n;i++){
        scanf("%d %d",&s,&d);
        a=(s+d)/2;
        b=s-a;
        if(a<0||b<0) printf("impossible\n");
        else printf("%d %d\n",a,b);
    }
    return 0;
}
//formula:
//s=a+b,d=a-b, for a>=b
//a=(s+d)/2, b=s-a
