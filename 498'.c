//http://uva.onlinejudge.org/external/102/10268.html
#include <stdio.h>
#include <string.h>
#include <math.h>

void derivative(int,int,int []);
int main(){
    int x,i;
    int a[100000];
    while(scanf("%d",&x)!=EOF){
        for(i=0;;i++){
            scanf("%d",&a[i]);
            if(getchar()=='\n') break;
        }
        derivative(x,i,a);
    }
    return 0;
}

void derivative(int x, int length, int array[]){
    int sum=0;
    int i;
    for(i=0;i<length;i++){
        sum+=(int)array[i]*(length-i)*pow(x,length-i-1);
    }
    printf("%d\n",sum);
}
