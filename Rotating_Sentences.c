//http:uva.onlinejudge.org/external/4/490.html
#include <stdio.h>
#include <string.h>

char map[100][100];
int main(){
	int row=0;
	int len_max=0;
	int len[100]={0}; //store the length of the each row
	int i,j;
	while(row<5){
		gets(map[row]);
		len[row]=strlen(map[row]);
		if(strlen(map[row])>len_max){
			len_max=strlen(map[row]);
		}
		row+=1;
	}
	//transpose char matrix.shape=(len_max,row)
	for(i=0;i<len_max;i++){
		for(j=0;j<row;j++){
			if(i<len[row-1-j]){
				printf("%c",map[row-1-j][i]); //-1:index from 0
			}
			else{
				printf(" ");
			}
		}
		printf("\n");
	}
	
	return 0;
}
