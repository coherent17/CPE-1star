//http://uva.onlinejudge.org/external/107/10783.html
#include <stdio.h>

int main(){
    int n,a,b;
    int i;
    scanf("%d",&n);
    for(i=0;i<n;i++){
        scanf("%d",&a);
        scanf("%d",&b);
        if(a%2==0 && b%2==0) printf("Case %d: %d\n",i+1,(a+b)*(b-a)/2/2);
        else if(a%2==1 && b%2==0) printf("Case %d: %d\n",i+1,((b-1-a)/2+1)*(a+b-1)/2);
        else if(a%2==1 && b%2==1) printf("Case %d: %d\n",i+1,((b-a)/2+1)*(a+b)/2);
        else if(a%2==0 && b%2==1) printf("Case %d: %d\n",i+1,((b-(a+1))/2+1)*(a+1+b)/2);
    }
    return 0;
}
