//http://uva.onlinejudge.org/external/100/10056.html
#include <stdio.h>
#include <math.h>

double probability(int,int,double);
int main(){
    int s,i,n,I;
    double p,temp;
    scanf("%d",&s);
    for(i=0;i<s;i++){
        scanf("%d %lf %d",&n,&p,&I);
        printf("%.4f\n",probability(n,I,p));
    }
    return 0;
}

double probability(int n,int I,double p){
    return pow(1-p,I-1)*p*1/(1-pow(1-p,n));
}

//formula:
//(1-p)^(i-1)*p+(1-p)^(n+i-1)*p+(1-p)^(2n+i-1)*p
//=(1-p)^(i-1)*p*[1+(1-p)^(n)+(1-p)^(2n)+...]
//=(1-p)^(i-1)*p*[1/(1-((1-p)^n))]
