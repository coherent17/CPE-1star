//http://uva.onlinejudge.org/external/102/10222.html
#include <stdio.h>
#include <string.h>
int main(){
    int i,j;
    const char table[]="`1234567890-=qwertyuiop[]\\asdfghjkl;\'zxcvbnm,./";
    char word[1000];
    gets(word);
    for(i=0;i<strlen(word);i++){
        for(j=0;j<strlen(table);j++){
            //all word convert into lower
            if(tolower(word[i])==table[j]){
                printf("%c",table[j-2]);
                break; //break the j loop
            }
            else if(word[i]==' '){
                printf(" ");
                break; //break the j loop
            }
        }
    }
    printf("\n");
    return 0;
}