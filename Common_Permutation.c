//http://uva.onlinejudge.org/external/102/10252.html
#include <stdio.h>
#include <string.h>

char a[1001],b[1001];
int i,j;
int main(){
    while(gets(a)!=EOF){
        int a_count[26]={0};
        int b_count[26]={0};

        for(i=0;i<strlen(a);i++){
           for(j=0;j<26;j++){
            if(a[i]-'a'==j){
                a_count[j]+=1;
            }
           }
        }
        gets(b);

        for(i=0;i<strlen(b);i++){
            for(j=0;j<26;j++){
                if(b[i]-'a'==j){
                    b_count[j]+=1;
                }
            }
        }
        //check for the subsequence
        for(i=0;i<26;i++){
            //appear in both sequence
            if(a_count[i]!=0 && b_count[i]!=0){
                printf("%c",i+'a');
            }
        }
        printf("\n");
    }
    return 0;
}
