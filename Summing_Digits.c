//http://uva.onlinejudge.org/external/113/11332.html
#include <stdio.h>

int main(){
    int n;
    int b;
    while(scanf("%d",&n)!=EOF){
        if(n==0){
            break;
        }
        else{
            b=f(n);
            while(b>=10){
                b=f(b);
            }
        }
        printf("%d\n",b);
    }
    return 0;
}

int f(int a){
    int value=0;
    while(a>=10){
        value+=a%10;
        a=(a-(a%10))/10;
    }
    value+=a;
    return value;
}
