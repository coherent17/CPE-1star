//http://uva.onlinejudge.org/external/110/11063.html
#include <stdio.h>
#include <stdlib.h>

int main(){
    int n;
    int i,j;
    int flag;//flag=0:B2-sequence, flag!=0:non B2-sequence
    int index;
    int t=0; //the index of the data
    while(scanf("%d",&n)!=EOF){
        t+=1;
        flag=0;
        index=0;
        int *array=malloc(n*sizeof(int));
        int *pair=malloc(n*(n+1)/2*sizeof(int));
        for(i=0;i<n;i++){
            scanf("%d",&array[i]);
        }
        //judge whether it is increase
        for(i=0;i<n-1;i++){
            if(array[i]>=array[i+1]) flag=1;
        }
        //calculate the pair sum
        if(flag==0){
            for(i=0;i<n;i++){
                for(j=i;j<n;j++){
                    pair[index]=array[i]+array[j];
                    index+=1;
                }
            }
            for(i=0;i<index;i++){
                for(j=i+1;j<index;j++){
                    if(pair[i]==pair[j]){
                        flag=1;
                        break;
                    }
                }
            }
        }
        if(flag==0) printf("Case #%d: It is a B2-Sequence\n",t);
        else printf("Case #%d: It is not a B2-Sequence\n",t);
    }
    return 0;
}
