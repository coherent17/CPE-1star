//http://uva.onlinejudge.org/100/10071.html
#include <stdio.h>

int main(){
    int v,t;
    while(scanf("%d %d",&v,&t)!=EOF){
        printf("%d\n",2*v*t);
    }
    return 0;
}
