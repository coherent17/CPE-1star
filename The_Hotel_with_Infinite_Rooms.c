//http://uva.onlinejudge.org/external/101/10170.html
#include <stdio.h>
#include <math.h>

int main(){
    int S;
    long long int D;
    long int ans;
    while(scanf("%d %lld",&S,&D)!=EOF){
        ans=(long long int)ceil((-1+sqrt(1-4*1*(-S*S+S-2*D)))/2);
        printf("%d\n",ans);
    }
    return 0;
}
//formula:
//S+(S+1)+(S+2)+...+N=(S+N)*(N-S+1)/2<=D
//N^2+N+(-S^2+S-2D)>=0
//N<=0.5*(-1+sqrt(1-4*1*(-S^2+S-2D)))
