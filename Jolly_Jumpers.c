//http://uva.onlinejudge.org/external/100/10038.html
#include <stdio.h>
#include <stdlib.h>

int main(){
    int len;
    int i;
    int flag=1;
    while(scanf("%d",&len)!=EOF){
        int *n=malloc(len*sizeof(int));
        int *d=malloc((len-1)*sizeof(int));
        int check[len-1];
        for(i=0;i<len-1;i++){
            check[i]=0;
        }
        for(i=0;i<len;i++){
            scanf("%d",&n[i]);
        }
        for(i=0;i<len-1;i++){
            d[i]=abs(n[i+1]-n[i]);
        }
        for(i=0;i<len-1;i++){
            check[d[i]-1]+=1;
        }
        for(i=0;i<len-1;i++){
            if(check[i]!=1){
                flag=0;
                break;
            }
        }
        if(flag==1){
            printf("Jolly\n");
        }
        else{
            printf("Not jolly\n");
        }
        free(n);
        free(d);
    }
    return 0;
}
