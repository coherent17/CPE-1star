//http://uva.onlinejudge.org/external/120/12019.html
#include <stdio.h>

//note that 2010/12/31 is Friday
int main(){
    int n,i,j,month,day,flag;
    int day_in_month[]={31,28,31,30,31,30,31,31,30,31,30};
    //%7=0~%7=6
    char week[7][10]={"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};
    scanf("%d",&n);
    for(i=0;i<n;i++){
        flag=5;//begin from Friday
        scanf("%d %d",&month,&day);
        for(j=1;j<month;j++){
            flag+=day_in_month[j-1];
        }
        flag+=day;
        flag=flag%7;
        printf("%s\n",week[flag]);
    }
    return 0;
}
