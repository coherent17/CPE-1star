//http://uva.onlinejudge.org/external/100/10008.html
#include <stdio.h>
#include <string.h>
int main(){
    int n,total;
    int i,j;
    char string[1000];
    int count[26]={0};
    scanf("%d",&n);
    getchar();
    total=0;
    for(i=0;i<n;i++){
        gets(string);
        for(j=0;j<strlen(string);j++){
            if(string[j]>='a'&&string[j]<='z'){
                count[string[j]-'a']++;
                total+=1;
            }
            else if(string[j]>='A'&&string[j]<='Z'){
                count[string[j]-'A']++;
                total+=1;
            }
        }
    }
    for(i=total;i>=1;i--){
        for(j=0;j<26;j++){
            if(count[j]==i){
                printf("%c %d\n",j+'A',i);
            }
        }
    }
    return 0;
}