//http://uva.onlinejudge.org/external/102/10222.html
#include <stdio.h>
#include <string.h>

int main(){
    int i,j;
    const char table[]="`1234567890-=qwertyuiop[]\\asdfghjkl;\'zxcvbnm,./";
    char word[1000];
    char *ptr;
    ptr=&table;//store the begin address of the table
    gets(word);
    //all word transfer into lower
    for(i=0;i<strlen(word);i++){
        word[i]=tolower(word[i]);
        ptr=strchr(table,word[i]);//return the address of the word in the table
        if(ptr){
            printf("%c",*(ptr-2));
        }
        else{
            printf(" ");
        }
    }
    return 0;
}
